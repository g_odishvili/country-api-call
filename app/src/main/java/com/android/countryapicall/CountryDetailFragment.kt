package com.android.countryapicall

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.android.countryapicall.databinding.FragmentCountryDetailBinding
import com.android.countryapicall.model.Country
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou


class CountryDetailFragment : Fragment() {
    private lateinit var binding: FragmentCountryDetailBinding


    companion object{
        const val NAME= "Name: "
        const val CAPITAL= "Capital: "
        const val TIMEZONES= "Timezones: "
        const val NATIVE_NAME= "Native Name: "
        const val POPULATION= "Population: "

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentCountryDetailBinding.inflate(
            inflater, container, false
        )
        init()
        return binding.root
    }

    private fun init() {
        val model: Country? = arguments?.getParcelable("country")
        binding.ivImage.loadSvg(model?.photoUrl)
        binding.tvCapital.text = CAPITAL + model!!.capital
        binding.tvFirstName.text = NAME + model!!.name
        binding.tvNativeName.text = NATIVE_NAME + model!!.nativeName
        binding.tvPopulation.text = POPULATION + model!!.population
        binding.tvTimezone.text = TIMEZONES+ model!!.timezones[0]

    }

    fun ImageView.loadSvg(url: String?) {
        GlideToVectorYou
            .init()
            .with(this.context)
            .setPlaceHolder(R.drawable.ic_launcher_background, R.drawable.ic_launcher_foreground)
            .load(Uri.parse(url), this)
    }
}