package com.android.countryapicall

import com.android.countryapicall.model.Country
import retrofit2.Response
import retrofit2.http.GET


interface CountryApi {

    @GET("rest/v2/all?fields=name;flag;capital;timezones;population;nativeName")
    suspend fun listCountries(): Response<List<Country>>

}