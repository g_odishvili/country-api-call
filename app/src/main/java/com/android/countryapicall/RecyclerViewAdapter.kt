package com.android.countryapicall

import android.annotation.SuppressLint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.android.countryapicall.databinding.CountryLayoutBinding
import com.android.countryapicall.model.Country
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou


class RecyclerViewAdapter(
    private val countryList: MutableList<Country>,
    private val countryListener: CountryListener
) : RecyclerView.Adapter<RecyclerViewAdapter.CountryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        return CountryViewHolder(CountryLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind()
    }


    override fun getItemCount(): Int  =countryList.size


    inner class CountryViewHolder(private val binding: CountryLayoutBinding)
        : RecyclerView.ViewHolder(binding.root){
        fun bind() {
            val model: Country = countryList[absoluteAdapterPosition]
            binding.title.text = model.name
            binding.image.loadSvg(model.photoUrl)
            binding.root.setOnClickListener {
                countryListener.onClickListener(absoluteAdapterPosition)
            }
        }

    }

    fun ImageView.loadSvg(url: String?) {
        GlideToVectorYou
            .init()
            .with(this.context)
            .setPlaceHolder(R.drawable.ic_launcher_background, R.drawable.ic_launcher_foreground)
            .load(Uri.parse(url), this)
    }
}