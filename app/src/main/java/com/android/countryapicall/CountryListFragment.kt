package com.android.countryapicall

import android.os.Bundle
import android.util.Log.i
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.android.countryapicall.databinding.FragmentCountryListBinding
import com.android.countryapicall.model.Country
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CountryListFragment : Fragment() {

    private lateinit var binding: FragmentCountryListBinding
    private lateinit var adapter: RecyclerViewAdapter
    private var fetchedCountries: MutableList<Country> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

            binding = FragmentCountryListBinding.inflate(
                inflater, container, false
            )
        init()

            CoroutineScope(Dispatchers.IO).launch {
                populateList()

                CoroutineScope(Dispatchers.Main).launch {
                    adapter.notifyDataSetChanged();
                }
            }



        return binding.root

    }


    private fun init() {

        adapter = RecyclerViewAdapter(fetchedCountries,object : CountryListener{
            override fun onClickListener(position: Int) {
                val bundleOf = bundleOf("country" to fetchedCountries[position])
                findNavController().navigate(R.id.action_countryListFragment_to_countryDetailFragment,
                    bundleOf
                )
            }
        })
        binding.recyclerView.layoutManager = GridLayoutManager(context,2)
        binding.recyclerView.adapter = adapter
    }


    private suspend fun populateList(){
        val result =  CountryService.retrofit().listCountries()
        if(result.isSuccessful){
            fetchedCountries.addAll( result.body() as MutableList<Country>)
        }
    }
}