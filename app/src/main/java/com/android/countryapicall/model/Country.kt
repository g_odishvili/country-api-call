package com.android.countryapicall.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Country (
    var name: String = "",
    @SerializedName("flag") var photoUrl:String = "",
    var capital:String ="",
    var population: Int = 0,
    var nativeName: String = "",
    var timezones: Array<String> = arrayOf()
    ) : Parcelable
